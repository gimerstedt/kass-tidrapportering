from node:11-alpine
copy app /app

workdir /app/client
run yarn install && yarn build

workdir /app
run yarn install

cmd yarn start
