import { DateTime } from 'luxon'

export const getSaturdayDate = (d: number = Date.now()) =>
  DateTime.fromMillis(d)
    .endOf('week')
    .minus({ days: 1 })
    .toFormat('dd-MM-yyyy')

export const exit = (reason: string, code: number = 0) => {
  console.log('exit: ' + reason)
  process.exit(code)
}

export const parseArgs = (argv: string[]) => {
  const arg = argv[2]
  const arr: number[] = JSON.parse(arg)
  if (arr.length !== 6 || arr[5] > 1 || arr[5] < 0) throw Error(argError)
  const ratio = arr.splice(5, 1)[0]
  return {
    ratio: ratio,
    hours: arr,
  }
}

export const argError = `bad input, 5 hours and the ratio eg. [8,8,8,8,8,0.5]`
