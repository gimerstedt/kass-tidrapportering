import { cells, sheet, template } from '../config.json'
import { getSaturdayDate } from './util'
import { loadWorkbook, saveWorkbook, setValue, genFilename } from './workbook'

const readAndSetValues = async (hours: number[], ratio: number) => {
  const wb = await loadWorkbook(template)

  setValue(wb, sheet, cells.periodEndDate, getSaturdayDate())

  const { cols, rows } = cells.weekdays
  for (let i = 0; i < hours.length; i++) {
    const p1 = hours[i] * ratio
    const p2 = hours[i] - p1

    const cell1 = cols[i] + rows[0]
    const cell2 = cols[i] + rows[1]

    setValue(wb, sheet, cell1, p1)
    setValue(wb, sheet, cell2, p2)
  }
  return wb
}

export const writeXlxs = async (hours: number[], ratio: number) => {
  const wb = await readAndSetValues(hours, ratio)
  await saveWorkbook(wb, genFilename())
}

export const getXlxs = async (hours: number[], ratio: number) => {
  const wb = await readAndSetValues(hours, ratio)
  const data = await wb.outputAsync()
  return {
    data,
    name: genFilename(),
  }
}
