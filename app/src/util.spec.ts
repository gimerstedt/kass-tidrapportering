import { getSaturdayDate } from './util'

describe('get-saturday-date', () => {
  it('works at time of writing', () => {
    const nov12th2018 = 1542054093000 // mon
    expect(getSaturdayDate(nov12th2018)).toEqual('17-11-2018')
  })

  it('works last week', () => {
    const nov10th2018 = 1541882097000 // sat
    expect(getSaturdayDate(nov10th2018)).toEqual('10-11-2018')
  })

  it('works some time last year', () => {
    const feb22nd2017 = 1487763297000 // wed
    expect(getSaturdayDate(feb22nd2017)).toEqual('25-02-2017')
  })

  it('works some time in the future', () => {
    const july10th2050 = 2541065697000 // sun
    expect(getSaturdayDate(july10th2050)).toEqual('09-07-2050')
  })
})
