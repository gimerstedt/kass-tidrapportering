import { DateTime } from 'luxon'
const x = require('xlsx-populate')

export const loadWorkbook = (wb: string) => x.fromFileAsync(wb)

export const saveWorkbook = (wb: any, filename: string) =>
  wb.toFileAsync(filename)

export const setValue = (
  wb: any,
  sheet: string,
  cell: string,
  value: string | number,
) => {
  if (value === null || value === undefined) return
  wb.sheet(sheet).cell(cell).value(value)
}

export const genFilename = (d: number = Date.now()) =>
  DateTime.fromMillis(d).toFormat('yyyy-MM-dd (wW)') + '.xlsx'
