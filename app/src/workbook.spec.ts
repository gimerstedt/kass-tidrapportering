import { genFilename } from './workbook'

describe('genFilename', () => {
  it('generates filename', () => {
    const nov12th2018 = 1542054093000
    const r = genFilename(nov12th2018)
    expect(r).toEqual('2018-11-12 (w46).xlsx')
  })
})
