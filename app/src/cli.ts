import { writeXlxs } from './main'
import { exit, parseArgs } from './util'

const cli = async () => {
  try {
    const { hours, ratio } = parseArgs(process.argv)
    await writeXlxs(hours, ratio)
  } catch (e) {
    exit(e, 1)
  }
}

cli()
