import { Request, Response } from 'express'
import { argError } from './util'
import { writeXlxs, getXlxs } from './main'

interface IBody {
  hours: number[]
  ratio: number
}

export const postRoute = async (req: Request, res: Response) => {
  const { hours, ratio } = req.body
  if (hours.length !== 5 || ratio > 1 || ratio < 0)
    return res.status(400).send({ error: argError })
  const { name, data } = await getXlxs(hours, ratio)
  res.attachment(name)
  return res.send(data)
}
