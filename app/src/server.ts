import express from 'express'
import { postRoute } from './routes'
import bodyParser from 'body-parser'
import cors from 'cors'

export const server = express()
  .use(cors())
  .use(bodyParser.json())
  .use(express.static('./client/build'))
  .post('/api', postRoute)
