import { server } from './server'
import { port } from '../config.json'

server.listen(port)
console.log('listening on port:', port)
