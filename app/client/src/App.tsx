import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Form from './Form'

export const App = () => {
  return (
    <div className="App">
      <Form />
    </div>
  )
}

export default App
