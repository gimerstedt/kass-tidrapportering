import { saveAs } from 'file-saver'
import React, { ChangeEvent, useState } from 'react'
import './Form.css'
import { DateTime } from 'luxon'

interface IState {
  hours: (string | number)[]
  ratio: number | string
}

const initialState: IState = {
  hours: [8, 8, 8, 8, 8],
  ratio: 0.5,
}

const genFilename = () => DateTime.local().toFormat('yyyy-MM-dd (wW)') + '.xlsx'

export default () => {
  const [state, setState] = useState(initialState)

  const createDayHandler = (idx: number) => (
    e: ChangeEvent<HTMLInputElement>,
  ) => {
    const hours = [...state.hours]
    hours[idx] = e.target.value ? parseFloat(e.target.value) : ''
    setState({
      ...state,
      hours,
    })
  }

  const ratioHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const ratio = e.target.value ? parseFloat(e.target.value) : ''
    setState({
      ...state,
      ratio,
    })
  }

  // const submit = (e: MouseEvent<HTMLInputElement>) => {
  const submit = (e: any) => {
    e.preventDefault()
    fetch('/api', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        hours: state.hours,
        ratio: state.ratio,
      }),
    })
      .then(d => d.blob())
      .then(b => {
        saveAs(b, genFilename())
      })
  }

  return (
    <form className="Form">
      <div>
        {state.hours.map((h, i) => (
          <input
            key={i}
            type="number"
            value={h}
            onChange={createDayHandler(i)}
          />
        ))}
      </div>
      <div>
        <input type="number" value={state.ratio} onChange={ratioHandler} />
      </div>
      <input type="submit" onClick={submit} />
    </form>
  )
}
